
public class Board {
	private char board[][] = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
	private Player X;
	private Player O;   
	private Player winner;
	private Player current;
	private int turnCount;
	private String welcome;

	Board(Player X , Player O) {
		this.X = X;
		this.O = O;
		winner = null;
		current = new Player('X');
		turnCount = 1;
		welcome = "Start Game OX Round "+turnCount;
	}

	public boolean setPosition(String r, String c) {
		try {
			int R = Integer.parseInt(r) ;
			int C = Integer.parseInt(c) ;
			if (R > 2 || R < 0 || C < 0 || C > 2) {
				System.err.println("Row and Column must be number 1 - 3");
				showBoard();
				return false;
			} else if (board[R][C] != (' ')) {
				System.err.println("Row " + (R + 1) + " and Column " + (C + 1) + " can't choose again");
				showBoard();
				return false;
			}
			board[R][C] = current.getName();
			return true;
		} catch (Exception a) {
			System.err.println("Row and Column must be number");
			showBoard();
			return false;
		}
	}

	public String getWelcome() {
		return welcome;
	}

	public Player getWinner() {
		return winner;
	}

	public Player getCurrent() {
		return current;
	}
        public char getBoardPosition (int row , int col){
            return board[row][col];
                   
        }

	public void showBoard() {
		System.out.println("  1 2 3 ");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < 3; j++) {
				System.out.print("|" + board[i][j]);
			}
			System.out.println("|");
		}
	}

	public boolean checkWin() {
		winner = current;
		for (int i = 0; i < 3; i++) {
			if (board[i][0] == current.getName() && board[i][1] == current.getName()
					&& board[i][2] == current.getName()) {
				return true;
			}
			if (board[0][i] == current.getName() && board[1][i] == current.getName()
					&& board[2][i] == current.getName()) {
				return true;
			}
		}
		if (board[0][0] == current.getName() && board[1][1] == current.getName() && board[2][2] == current.getName()) {
			return true;
		}
		if (board[0][2] == current.getName() && board[1][1] == current.getName() && board[2][0] == current.getName()) {
			return true;
		}
		winner = null ;
		return false;
	}
        
        
	
	public boolean checkDraw() {
		boolean chk = false;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board[i][j] == ' ') {
					chk = true;
				}
			}
		}
		if (chk == false) {
			return true ;
		}
		return false ;
	}

	public int getturnCount() {
		return turnCount;
	}

	public void switchTurn() {
		if (current.getName() == 'X') {
			current.setName('O');
		} else {
			current.setName('X');
		}
	}

	public boolean isFinish() {
		if (checkWin()) {
			return true;
		}else if (checkDraw()) {
			return true;
		}
		
		return false;
	}
	public void clearBoard() {
		turnCount++ ;
		current.setName('X');
		welcome = "Start Game OX Round "+turnCount;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				board[i][j] = ' ';
			}
		}
	}
}
